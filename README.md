# Release Notes template

This GitHub repository contains the content for the Release Notes for the GeoPackage Related Tables Extension with Corrigendum, OGC 18-000r1.

The repo is organized as follows:

* index.adoc - nothing in this document
* release_notes - the main document content, organized in multiple sections
  * [Release Notes](release_notes/rn.adoc)

